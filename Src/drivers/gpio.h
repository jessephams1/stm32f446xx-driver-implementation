#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "stm32f446xx.h"

typedef struct {
    GPIO_Register_Definition_s *port;
    uint8_t pin;
} gpio__s;

typedef enum {
    GPIO__MODE_INPUT = 0U,
    GPIO__MODE_OUTPUT,
    GPIO__MODE_ALTERNATE_FUNCTION,
    GPIO__MODE_ANALOG,
} gpio__mode_e;

typedef enum {
    GPIO__OUTPUT_SPEED_LOW = 0U,
    GPIO__OUTPUT_SPEED_MEDIUM,
    GPIO__OUTPUT_SPEED_FAST,
    GPIO__OUTPUT_SPEED_HIGH,
} gpio__output_speed_e;

typedef enum {
    GPIO__PUPD_NONE = 0U,
    GPIO__PUPD_PULL_UP,
    GPIO__PUPD_PULL_DOWN,
} gpio__pupd_e;

typedef enum {
    GPIO__OUTPUT_TYPE_PUSH_PULL = 0U,
    GPIO__OUTPUT_TYPE_OPEN_DRAIN,
} gpio__output_type_e;

typedef enum {
    GPIO__WRITE_VALUE_LOW = 0U,
    GPIO__WRITE_VALUE_HIGH,
} gpio__write_value_e;


/**
 * PUBLIC FUNCTIONS
 */

void gpio__set_as_default_output(gpio__s gpio);

void gpio__set_mode(gpio__s gpio, gpio__mode_e mode);
void gpio__set_output_speed(gpio__s gpio, gpio__output_speed_e speed);
void gpio__set_pupd(gpio__s gpio, gpio__pupd_e pupd);
void gpio__set_output_type(gpio__s gpio, gpio__output_type_e output_type);

bool gpio__read(gpio__s gpio);
void gpio__write(gpio__s gpio, gpio__write_value_e value);