#include "gpio.h"

void gpio__set_as_default_output(gpio__s gpio) {
    GPIOA_PCLK_EN();
    gpio__set_output_speed(gpio, GPIO__OUTPUT_SPEED_MEDIUM);
    gpio__set_pupd(gpio, GPIO__PUPD_PULL_DOWN);
    gpio__set_mode(gpio, GPIO__MODE_OUTPUT);
}

void gpio__set_mode(gpio__s gpio, gpio__mode_e mode) {
    // clear mode register bits
    gpio.port->MODER &= ~(3U << (2U * gpio.pin));   // Note: we multiply by two because each pin requires 2 bits to set

    // set mode register bits
    gpio.port->MODER |= ((uint8_t)mode << (2U * gpio.pin));
}

void gpio__set_output_speed(gpio__s gpio, gpio__output_speed_e speed) {
    // clear output speed register bits
    gpio.port->OSPEEDER &= ~(3U << (2U * gpio.pin));

    // set output speed register bits
    gpio.port->OSPEEDER |= ((uint8_t)speed << (2U * gpio.pin));
}

void gpio__set_pupd(gpio__s gpio, gpio__pupd_e pupd) {
    // clear pull-up pull-down register bits
    gpio.port->PUPDR &= ~(3U << (2U * gpio.pin));

    // set pull-up pull-down register bits
    gpio.port->PUPDR |= ((uint8_t)pupd << (2U * gpio.pin));
}

void gpio__set_output_type(gpio__s gpio, gpio__output_type_e output_type) {
    // clear output type register bits
    gpio.port->OTYPER &= ~(1U << gpio.pin);

    // set output type register bits
    gpio.port->OTYPER |= ((uint8_t)output_type << gpio.pin);
}

bool gpio__read(gpio__s gpio) {
    bool pin_is_high = false;

    if (gpio.port->IDR & (1U << gpio.pin)) {
        pin_is_high = true;
    }

    return pin_is_high;
}

void gpio__write(gpio__s gpio, gpio__write_value_e value) {
    // clear output data register bits
    gpio.port->ODR &= ~(1U << gpio.pin);

    // set output data register bits
    gpio.port->ODR |= ((uint8_t)value << gpio.pin);
}