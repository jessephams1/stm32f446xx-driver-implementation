#pragma once

#include <stdint.h>

/**
 * 2.2.2 Memory map and register boundary addresses
 */
#define FLASH_BASE_ADDRESS      0x08000000UL
#define SRAM1_BASE_ADDRESS      0x20000000UL
#define SRAM2_BASE_ADDRESS      0x2001C000UL
#define ROM_BASE_ADDRESS        0x1FFF0000UL
#define SRAM                    SRAM1_BASE_ADDRESS

#define APB1_BASE_ADDRESS       0x40000000UL
#define APB2_BASE_ADDRESS       0x40010000UL
#define AHB1_BASE_ADDRESS       0x40020000UL
#define AHB2_BASE_ADDRESS       0x50000000UL
#define AHB3_BASE_ADDRESS       0x60000000UL

#define GPIO_A_BASE_ADDRESS     AHB1_BASE_ADDRESS
#define GPIO_B_BASE_ADDRESS     (AHB1_BASE_ADDRESS + 0x0400UL)
#define GPIO_C_BASE_ADDRESS     (AHB1_BASE_ADDRESS + 0x0800UL)
#define GPIO_D_BASE_ADDRESS     (AHB1_BASE_ADDRESS + 0x0C00UL)
#define GPIO_E_BASE_ADDRESS     (AHB1_BASE_ADDRESS + 0x1000UL)
#define GPIO_F_BASE_ADDRESS     (AHB1_BASE_ADDRESS + 0x1400UL)
#define GPIO_G_BASE_ADDRESS     (AHB1_BASE_ADDRESS + 0x1800UL)
#define GPIO_H_BASE_ADDRESS     (AHB1_BASE_ADDRESS + 0x1C00UL)

#define SPI1_BASE_ADDRESS       (APB2_BASE_ADDRESS + 0x3000UL)
#define SPI2_BASE_ADDRESS       (APB1_BASE_ADDRESS + 0X3800UL)
#define SPI3_BASE_ADDRESS       (APB1_BASE_ADDRESS + 0X3C00UL)
#define SPI4_BASE_ADDRESS       (APB2_BASE_ADDRESS + 0X3400UL)

#define USART1_BASE_ADDRESS     (APB2_BASE_ADDRESS + 0x1000UL)
#define USART2_BASE_ADDRESS     (APB1_BASE_ADDRESS + 0x4400UL)
#define USART3_BASE_ADDRESS     (APB1_BASE_ADDRESS + 0x4800UL)
#define UART4_BASE_ADDRESS      (APB1_BASE_ADDRESS + 0x4C00UL)
#define UART5_BASE_ADDRESS      (APB1_BASE_ADDRESS + 0x5000UL)
#define USART6_BASE_ADDRESS     (APB2_BASE_ADDRESS + 0x1400UL)

#define I2C1_BASE_ADDRESS       (APB1_BASE_ADDRESS + 0x5400UL)
#define I2C2_BASE_ADDRESS       (APB1_BASE_ADDRESS + 0x5800UL)
#define I2C3_BASE_ADDRESS       (APB1_BASE_ADDRESS + 0x5C00UL)

#define EXTI_BASE_ADDRESS       (APB2_BASE_ADDRESS + 0x3C00UL)
#define RCC_BASE_ADDRESS        (AHB1_BASE_ADDRESS + 0x3800UL)
#define SYSCFG_BASE_ADDRESS     (APB2_BASE_ADDRESS + 0x3800UL)

/**
 * 6.3.28 RCC register map
 * Note: We will use this to enable the clock for our peripherals
 */
typedef struct {
    volatile uint32_t CR;
    volatile uint32_t PLLCFGR;
    volatile uint32_t CFGR;
    volatile uint32_t CIR;
    volatile uint32_t AHB1RSTR;
    volatile uint32_t AHB2RSTR;
    volatile uint32_t AHB3RSTR;
    uint32_t reserved1;
    volatile uint32_t APB1RSTR;
    volatile uint32_t APB2RSTR;
    uint32_t reserved2;
    uint32_t reserved3;
    volatile uint32_t AHB1ENR;
    volatile uint32_t AHB2ENR;
    volatile uint32_t AHB3ENR;
    uint32_t reserved4;
    volatile uint32_t APB1ENR;
    volatile uint32_t APB2ENR;
    uint32_t reserved5;
    uint32_t reserved6;
} RCC_Register_Definition_s;

#define RCC ((RCC_Register_Definition_s*)RCC_BASE_ADDRESS)

/**
 * 7.4.11 GPIO register map
 * Note: We are making these member variables volatile because the nature of these
 * GPIO pins are highly volatile (such as the input data register, which will get
 * updated for each AHB clock)
 */
typedef struct {
    volatile uint32_t MODER;
    volatile uint32_t OTYPER;
    volatile uint32_t OSPEEDER;
    volatile uint32_t PUPDR;
    volatile uint32_t IDR;
    volatile uint32_t ODR;
    volatile uint32_t BSRR;
    volatile uint32_t LCKR;
    volatile uint32_t AFRL;
    volatile uint32_t AFRH;
} GPIO_Register_Definition_s;

#define GPIO_A ((GPIO_Register_Definition_s*)GPIO_A_BASE_ADDRESS)
#define GPIO_B ((GPIO_Register_Definition_s*)GPIO_B_BASE_ADDRESS)
#define GPIO_C ((GPIO_Register_Definition_s*)GPIO_C_BASE_ADDRESS)
#define GPIO_D ((GPIO_Register_Definition_s*)GPIO_D_BASE_ADDRESS)
#define GPIO_E ((GPIO_Register_Definition_s*)GPIO_E_BASE_ADDRESS)
#define GPIO_F ((GPIO_Register_Definition_s*)GPIO_F_BASE_ADDRESS)
#define GPIO_G ((GPIO_Register_Definition_s*)GPIO_G_BASE_ADDRESS)
#define GPIO_H ((GPIO_Register_Definition_s*)GPIO_H_BASE_ADDRESS)

/**
 * Enable Peripheral Clock
 * 6.3.1x clock enable register
 */
#define GPIOA_PCLK_EN()         (RCC->AHB1ENR |= (1U << 0U))
#define GPIOB_PCLK_EN()         (RCC->AHB1ENR |= (1U << 1U))
#define GPIOC_PCLK_EN()         (RCC->AHB1ENR |= (1U << 2U))
#define GPIOD_PCLK_EN()         (RCC->AHB1ENR |= (1U << 3U))
#define GPIOE_PCLK_EN()         (RCC->AHB1ENR |= (1U << 4U))
#define GPIOF_PCLK_EN()         (RCC->AHB1ENR |= (1U << 5U))
#define GPIOG_PCLK_EN()         (RCC->AHB1ENR |= (1U << 6U))
#define GPIOH_PCLK_EN()         (RCC->AHB1ENR |= (1U << 7U))

#define SPI1_PCLK_EN()          (RCC->APB2ENR |= (1U << 12U))
#define SPI2_PCLK_EN()          (RCC->APB1ENR |= (1U << 14U))
#define SPI3_PCLK_EN()          (RCC->APB1ENR |= (1U << 15U))
#define SPI4_PCLK_EN()          (RCC->APB2ENR |= (1U << 13U))

#define USART1_PCLK_EN()        (RCC->APB2ENR |= (1U << 4U))
#define USART2_PCLK_EN()        (RCC->APB1ENR |= (1U << 17U))
#define USART3_PCLK_EN()        (RCC->APB1ENR |= (1U << 18U))
#define UART4_PCLK_EN()         (RCC->APB1ENR |= (1U << 19U))
#define UART5_PCLK_EN()         (RCC->APB1ENR |= (1U << 20U))
#define USART6_PCLK_EN()        (RCC->APB2ENR |= (1U << 5U))

#define I2C1_PCLK_EN()          (RCC->APB1ENR |= (1U << 21U))
#define I2C2_PCLK_EN()          (RCC->APB1ENR |= (1U << 22U))
#define I2C3_PCLK_EN()          (RCC->APB1ENR |= (1U << 23U))

/**
 * Disable Peripheral Clock
 */
#define GPIOA_PCLK_DISABLE()         (RCC->AHB1ENR &= ~(1U << 0U))
#define GPIOB_PCLK_DISABLE()         (RCC->AHB1ENR &= ~(1U << 1U))
#define GPIOC_PCLK_DISABLE()         (RCC->AHB1ENR &= ~(1U << 2U))
#define GPIOD_PCLK_DISABLE()         (RCC->AHB1ENR &= ~(1U << 3U))
#define GPIOE_PCLK_DISABLE()         (RCC->AHB1ENR &= ~(1U << 4U))
#define GPIOF_PCLK_DISABLE()         (RCC->AHB1ENR &= ~(1U << 5U))
#define GPIOG_PCLK_DISABLE()         (RCC->AHB1ENR &= ~(1U << 6U))
#define GPIOH_PCLK_DISABLE()         (RCC->AHB1ENR &= ~(1U << 7U))

#define SPI1_PCLK_DISABLE()          (RCC->APB2ENR &= ~(1U << 12U))
#define SPI2_PCLK_DISABLE()          (RCC->APB1ENR &= ~(1U << 14U))
#define SPI3_PCLK_DISABLE()          (RCC->APB1ENR &= ~(1U << 15U))
#define SPI4_PCLK_DISABLE()          (RCC->APB2ENR &= ~(1U << 13U))

#define USART1_PCLK_DISABLE()        (RCC->APB2ENR &= ~(1U << 4U))
#define USART2_PCLK_DISABLE()        (RCC->APB1ENR &= ~(1U << 17U))
#define USART3_PCLK_DISABLE()        (RCC->APB1ENR &= ~(1U << 18U))
#define UART4_PCLK_DISABLE()         (RCC->APB1ENR &= ~(1U << 19U))
#define UART5_PCLK_DISABLE()         (RCC->APB1ENR &= ~(1U << 20U))
#define USART6_PCLK_DISABLE()        (RCC->APB2ENR &= ~(1U << 5U))

#define I2C1_PCLK_DISABLE()          (RCC->APB1ENR &= ~(1U << 21U))
#define I2C2_PCLK_DISABLE()          (RCC->APB1ENR &= ~(1U << 22U))
#define I2C3_PCLK_DISABLE()          (RCC->APB1ENR &= ~(1U << 23U))